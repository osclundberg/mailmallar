jQuery(document).ready(function($){

	function printTemplates(){
		var footer = $('#footer').html();
		var header = $('#header').html();
		var conversation = $('#conversation').html();
		var issue = $('#issue').html();
		var review = $('#review').html();

		var conversationItem = $('#conversation-item').html();
		var costumer = $('#costumer').html();
		var developerNewPost = $('#developer-newpost').html();
		var firstPost = $('#costumer-firstpost').html();
		var issueCostumer = $('#issue-costumer').html();

		var footerdata = {
			phone: "010 - 762 44 40",
			email: "hej@angrycreative.se",
			facebook: "facebook.com/angrycreative"
		}
		var data = {};


		var developerData = {
			name: "Robin Björklund",
			img: "http://lek.oscarjohnson.me:1234/?mail=boberober@gmail.com",
			email: "robin@angrycreative.se",
			message: "magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatu"
		};

		var costumerName = "Sven Svensson";
		var costumerImg = "http://media.angrycreative.se/developer_red_45.jpg";

		var costumerData = {
			name: costumerName,
			img: costumerImg,
			email: "sven@svensson.se",
			message: "Lorem ipsum dolor sit amet, Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
		};

		var firstPostData = {
			name: costumerName,
			img: costumerImg,
			email: "sven@svensson.se",
			header: true,
			headerInfo: "Ärende #802 - Det här krånglar", 
			message: "Lorem ipsum dolor sit amet, Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
		};

		$('.header').html( Mustache.to_html(header, data) );

		$('.issue').html( Mustache.to_html(conversationItem, firstPostData) );	
		$('.conversation-history').html( Mustache.to_html(conversation, data) );

		$('.costumer').html( Mustache.to_html(conversationItem, costumerData) );
		$('.developer').html( Mustache.to_html(conversationItem, developerData) );
		$('.firstpost').html( Mustache.to_html(conversationItem, firstPostData) );
		$('.newpost').html( Mustache.to_html(conversationItem, developerData) );

		$('.review').html( Mustache.to_html(review, data) );	



		$('.footer').html( Mustache.to_html(footer, data) );
	}

	printTemplates();


	function createMenuItems(){
		var elstring = "<ul>";
		$('section').each(function(index, el){
			var h2 = $(el).find('h2:first-child').html();
			var id = $(el).attr('id');
			console.log();

			h2 = h2.replace(/Som (.*) vill jag/, '<span class="faded">($1)</span>');

			var ahref = '<li><a href="#'+id+'">'+h2+'</a></li>';

			elstring += ahref;
		});
		elstring += "</ul>";

		$('nav.menu').append(elstring);

	}
	createMenuItems();

	function dimConversation(){
		var theLength = $('.conversation-history ul li').size();
		$('.conversation-history ul li').each(function(index, el){
			if(index == 0){
				return;
			}
			if(index == theLength-2){
				return
			}
			//$(el).css({opacity: 0.5});

		});
		
	}
	dimConversation();



});
